using System;
using Nancy;
using Nancy.ModelBinding;
using System.Collections.Generic;
using Sharpility.Base;
using IO.Swagger.ERROR_UNKNOWN.Models;
using IO.Swagger.ERROR_UNKNOWN.Utils;
using NodaTime;

namespace IO.Swagger.ERROR_UNKNOWN.Modules
{ 

    /// <summary>
    /// Module processing requests of Api domain.
    /// </summary>
    public sealed class ApiModule : NancyModule
    {
        /// <summary>
        /// Sets up HTTP methods mappings.
        /// </summary>
        /// <param name="service">Service handling requests</param>
        public ApiModule(ApiService service) : base("")
        { 
            Post["/api/v1.0/playmate/openSync"] = parameters =>
            {
                var xClientSecret = Parameters.ValueOf<string>(parameters, Context.Request, "xClientSecret", ParameterType.Header);
                var applicationIdentifier = Parameters.ValueOf<string>(parameters, Context.Request, "applicationIdentifier", ParameterType.Undefined);
                var applicationName = Parameters.ValueOf<string>(parameters, Context.Request, "applicationName", ParameterType.Undefined);
                var person = Parameters.ValueOf<string>(parameters, Context.Request, "person", ParameterType.Undefined);
                var saveURL = Parameters.ValueOf<string>(parameters, Context.Request, "saveURL", ParameterType.Undefined);
                var returnURL = Parameters.ValueOf<string>(parameters, Context.Request, "returnURL", ParameterType.Undefined);
                var lenderCode = Parameters.ValueOf<string>(parameters, Context.Request, "lenderCode", ParameterType.Undefined);
                var documentationType = Parameters.ValueOf<string>(parameters, Context.Request, "documentationType", ParameterType.Undefined);
                var guidebookCode = Parameters.ValueOf<string>(parameters, Context.Request, "guidebookCode", ParameterType.Undefined);
                var relatedDates = Parameters.ValueOf<string>(parameters, Context.Request, "relatedDates", ParameterType.Undefined);
                var relatedProperties = Parameters.ValueOf<string>(parameters, Context.Request, "relatedProperties", ParameterType.Undefined);
                var isViewMode = Parameters.ValueOf<bool?>(parameters, Context.Request, "isViewMode", ParameterType.Undefined);
                var integrator = Parameters.ValueOf<System.IO.Stream>(parameters, Context.Request, "integrator", ParameterType.Undefined);
                var flat = Parameters.ValueOf<System.IO.Stream>(parameters, Context.Request, "flat", ParameterType.Undefined);
                Preconditions.IsNotNull(xClientSecret, "Required parameter: 'xClientSecret' is missing at 'POSTApiV20IntegrationOpenSync'");
                
                Preconditions.IsNotNull(applicationIdentifier, "Required parameter: 'applicationIdentifier' is missing at 'POSTApiV20IntegrationOpenSync'");
                
                Preconditions.IsNotNull(applicationName, "Required parameter: 'applicationName' is missing at 'POSTApiV20IntegrationOpenSync'");
                
                Preconditions.IsNotNull(person, "Required parameter: 'person' is missing at 'POSTApiV20IntegrationOpenSync'");
                
                Preconditions.IsNotNull(saveURL, "Required parameter: 'saveURL' is missing at 'POSTApiV20IntegrationOpenSync'");
                
                service.POSTApiV20IntegrationOpenSync(Context, xClientSecret, applicationIdentifier, applicationName, person, saveURL, returnURL, lenderCode, documentationType, guidebookCode, relatedDates, relatedProperties, isViewMode, integrator, flat);
                return new Response { ContentType = ""};
            };
        }
    }

    /// <summary>
    /// Service handling Api requests.
    /// </summary>
    public interface ApiService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context">Context of request</param>
        /// <param name="xClientSecret">Simpology assigned password for BasePlate Integration</param>
        /// <param name="applicationIdentifier">The unique identifier of the application in your CRM</param>
        /// <param name="applicationName">The name of the application in the CRM</param>
        /// <param name="person">Json object that contains details about the user making the open request.</param>
        /// <param name="saveURL">The URL where the integrator XML file will be stored when the application is saved.</param>
        /// <param name="returnURL">The URL where the user will be redirected when the application is closed. (optional, default to www.returnurl.com)</param>
        /// <param name="lenderCode">The code identifier of the lender that owns the guidebook on which the application will be opened. (optional, default to CDA2IA)</param>
        /// <param name="documentationType">The type of documentation for the application being opened. One of Full Doc, Lo Doc or No Doc. (optional, default to Full Doc)</param>
        /// <param name="guidebookCode">The name code identifier of the guidebook on which the application will be opened. (optional, default to EGBT-TWPTI)</param>
        /// <param name="relatedDates">Json object that contains details about the dates that are important for the application (Created Date for ex.). (optional, default to { &quot;Created Date&quot;: &quot;11-11-1990&quot; })</param>
        /// <param name="relatedProperties">Json object that contains details about other important data about the application (Opportunity Number for ex.). (optional, default to { &quot;Owner/s&quot;: {   &quot;FirstName&quot;: &quot;Catalin&quot;,   &quot;Surname&quot;: &quot;Gavan&quot;,   &quot;EmailAddress&quot;: &quot;catalingavan@simpology.ro&quot; })</param>
        /// <param name="isViewMode">Flag to indicate whether the application should be opened in view mode. View mode does not allow for any modifications to be made to the application data. (optional, default to false)</param>
        /// <param name="integrator">The file to be transformed from internal CRM format to Loanapp format. (optional)</param>
        /// <param name="flat">The Loanapp format xml file that is sent when opening an existing application. (optional)</param>
        /// <returns></returns>
        void POSTApiV20IntegrationOpenSync(NancyContext context, string xClientSecret, string applicationIdentifier, string applicationName, string person, string saveURL, string returnURL, string lenderCode, string documentationType, string guidebookCode, string relatedDates, string relatedProperties, bool? isViewMode, System.IO.Stream integrator, System.IO.Stream flat);
    }

    /// <summary>
    /// Abstraction of ApiService.
    /// </summary>
    public abstract class AbstractApiService: ApiService
    {
        public virtual void POSTApiV20IntegrationOpenSync(NancyContext context, string xClientSecret, string applicationIdentifier, string applicationName, string person, string saveURL, string returnURL, string lenderCode, string documentationType, string guidebookCode, string relatedDates, string relatedProperties, bool? isViewMode, System.IO.Stream integrator, System.IO.Stream flat)
        {
            POSTApiV20IntegrationOpenSync(xClientSecret, applicationIdentifier, applicationName, person, saveURL, returnURL, lenderCode, documentationType, guidebookCode, relatedDates, relatedProperties, isViewMode, integrator, flat);
        }

        protected abstract void POSTApiV20IntegrationOpenSync(string xClientSecret, string applicationIdentifier, string applicationName, string person, string saveURL, string returnURL, string lenderCode, string documentationType, string guidebookCode, string relatedDates, string relatedProperties, bool? isViewMode, System.IO.Stream integrator, System.IO.Stream flat);
    }

}
